#!/bin/bash

set -e

ROOT_DIR=$(git rev-parse --show-toplevel)
COMMON_DIR=${ROOT_DIR}/scm/common

source ${COMMON_DIR}/echo.sh

cd ${ROOT_DIR}
pwd

# check to use the cpplint
echo_func "[scm] cpplint checking" 0
FILES=$(find . \
	-type f \
	-not -path "./docs/*" \
	\( -name "*.cpp" -o -name "*.h" \)
)

for file in ${FILES}
do
	cpplint ${file}
	if [ "$?" -ne "0" ]
	then
		exit 1
	fi
done

echo_func "[scm] C++ language CI test done!" 0
