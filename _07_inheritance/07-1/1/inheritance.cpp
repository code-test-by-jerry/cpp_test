/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <iostream>

class Car {
public: // NOLINT
  explicit Car(int gas);
  int GetGasGauge(void);

private: // NOLINT
  int gasolineGauge;
};

class HybridCar : public Car {
public: // NOLINT
  HybridCar(int gas, int electric);
  int GetElecGauge(void);

private: // NOLINT
  int electricGauge;
};

class HybridWaterCar : public HybridCar {
public: // NOLINT
  HybridWaterCar(int gas, int electric, int water);
  void ShowCurrentGauge(void);

private: // NOLINT
  int waterGauge;
};

Car::Car(int gas) : gasolineGauge(gas) {}

int Car::GetGasGauge(void) { return gasolineGauge; }

HybridCar::HybridCar(int gas, int electric)
    : Car(gas), electricGauge(electric) {}

int HybridCar::GetElecGauge(void) { return electricGauge; }

HybridWaterCar::HybridWaterCar(int gas, int electric, int water)
    : HybridCar(gas, electric), waterGauge(water) {}

void HybridWaterCar::ShowCurrentGauge(void) {
  std::cout << "잔여 가솔린: " << GetGasGauge() << std::endl;
  std::cout << "잔여 전기량: " << GetElecGauge() << std::endl;
  std::cout << "잔여 워터량: " << waterGauge << std::endl;
}

int main(void) {
  HybridWaterCar *hybridWaterCar = new HybridWaterCar(1, 2, 3);
  hybridWaterCar->ShowCurrentGauge();

  delete hybridWaterCar;

  return 0;
}
