/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <cstring>
#include <iostream>

class MyFriendInfo {
public: // NOLINT
  MyFriendInfo(const char *name, const int age);
  ~MyFriendInfo(void);

  void ShowMyFriendInfo(void);

private: // NOLINT
  char *name;
  int age;
};

class MyFriendDetailInfo : public MyFriendInfo {
public: // NOLINT
  MyFriendDetailInfo(const char *name, int age, const char *addr,
                     const char *phone);
  ~MyFriendDetailInfo(void);

  void ShowMyFriendDetailINfo(void);

private: // NOLINT
  char *addr;
  char *phone;
};

MyFriendInfo::MyFriendInfo(const char *name, const int age) : age(age) {
  size_t nameLen = strlen(name) + 1;
  this->name = new char[nameLen];
  snprintf(this->name, nameLen, "%s", name);
}

MyFriendInfo::~MyFriendInfo(void) {
  if (this->name)
    delete[] this->name;
}

MyFriendDetailInfo::MyFriendDetailInfo(const char *name, int age,
                                       const char *addr, const char *phone)
    : MyFriendInfo(name, age) {

  size_t addrLen = strlen(addr) + 1;
  size_t phoneLen = strlen(phone) + 1;

  this->addr = new char[addrLen];
  this->phone = new char[phoneLen];

  snprintf(this->addr, addrLen, "%s", addr);
  snprintf(this->phone, phoneLen, "%s", phone);
}

MyFriendDetailInfo::~MyFriendDetailInfo(void) {
  if (this->addr)
    delete this->addr;

  if (this->phone)
    delete this->phone;
}

void MyFriendInfo::ShowMyFriendInfo(void) {
  std::cout << "이름: " << name << std::endl;
  std::cout << "나이: " << age << std::endl;
}

void MyFriendDetailInfo::ShowMyFriendDetailINfo(void) {
  ShowMyFriendInfo();
  std::cout << "주소: " << addr << std::endl;
  std::cout << "전화: " << phone << std::endl;
}

int main(void) {
  MyFriendDetailInfo *friends =
      new MyFriendDetailInfo("Lee, Jerry J", 35, "Seoul", "12345678");

  friends->ShowMyFriendDetailINfo();

  delete friends;

  return 0;
}
