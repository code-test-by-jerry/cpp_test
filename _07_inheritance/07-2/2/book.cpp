/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <cstring>
#include <iostream>

class Book {
public: // NOLINT
  Book(const char *title, const char *isbn, int price);

  void ShowBookInfo(void) const;

private: // NOLINT
  char *title;
  char *isbn;
  int price;
};

class EBook : public Book {
public: // NOLINT
  EBook(const char *title, const char *isbn, int price, const char *DRMKey);

  void ShowEBookInfo(void) const;

private: // NOLINT
  char *DRMKey;
};

Book::Book(const char *title, const char *isbn, int price) : price(price) {
  size_t titleLen = strlen(title) + 1;
  size_t isbnLen = strlen(isbn) + 1;

  this->title = new char[titleLen];
  this->isbn = new char[isbnLen];

  snprintf(this->title, titleLen, "%s", title);
  snprintf(this->isbn, isbnLen, "%s", isbn);
}

void Book::ShowBookInfo(void) const {
  std::cout << "제목: " << this->title << std::endl;
  std::cout << "ISBN: " << this->isbn << std::endl;
  std::cout << "가격: " << this->price << std::endl;
}

EBook::EBook(const char *title, const char *isbn, int price, const char *DRMKey)
    : Book(title, isbn, price) {
  size_t DRMKeyLen = strlen(DRMKey) + 1;
  this->DRMKey = new char[DRMKeyLen];
  snprintf(this->DRMKey, DRMKeyLen, "%s", DRMKey);
}

void EBook::ShowEBookInfo(void) const {
  ShowBookInfo();
  std::cout << "인증키: " << this->DRMKey << std::endl;
}

int main(void) {
  Book book("좋은 C++", "555-12345-890-0", 20000);
  book.ShowBookInfo();

  std::cout << std::endl;

  EBook ebook("좋은 C++", "555-12345-890-1", 10000, "fdx9w0i8kiw");
  ebook.ShowEBookInfo();

  return 0;
}
