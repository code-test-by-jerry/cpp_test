/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <iostream>

class Rectangle {
public: // NOLINT
  Rectangle(int posX, int posY);

  void ShowAreaInfo(void);

private: // NOLINT
  int posX;
  int posY;
};

class Square : public Rectangle {
public: // NOLINT
  explicit Square(int value);

private: // NOLINT
};

Rectangle::Rectangle(int posX, int posY) : posX(posX), posY(posY) {}

void Rectangle::ShowAreaInfo(void) {
  std::cout << "면적: " << posX * posY << std::endl;
}

Square::Square(int value) : Rectangle(value, value) {}

int main(void) {
  Rectangle rec(4, 3);
  rec.ShowAreaInfo();

  Square sqr(7);
  sqr.ShowAreaInfo();

  return 0;
}
