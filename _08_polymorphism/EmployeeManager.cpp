/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <cstring>
#include <iostream>

enum class RISK_LEVEL { RISK_C = 1, RISK_B, RISK_A };

class Employee {
public: // NOLINT
  explicit Employee(const char *name);
  virtual ~Employee();

  void ShowYourName(void) const;

  virtual int GetPay(void) const;
  virtual void ShowSalaryInfo(void) const;

private: // NOLINT
  char name[100];
};

class PermanentWorker : public Employee {
public: // NOLINT
  PermanentWorker(std::string name, int money);

  int GetPay(void) const;
  void ShowSalaryInfo(void) const;

private: // NOLINT
  int salary;
};

class TemporaryWorker : public Employee {
public: // NOLINT
  TemporaryWorker(std::string name, int pay);

  void AddWorkTime(int time);
  int GetPay(void) const;
  void ShowSalaryInfo(void) const;

private: // NOLINT
  int workTime;
  int payPerHour;
};

class SalesWorker : public PermanentWorker {
public: // NOLINT
  SalesWorker(std::string name, int money, double ratio);

  void AddSalesResult(int value);
  int GetPay(void) const;
  void ShowSalaryInfo(void) const;

private: // NOLINT
  int salesResult;
  double bonusRatio;
};

class ForeignSalesWorker : public SalesWorker {
public: // NOLINT
  ForeignSalesWorker(std::string name, int money, double ratio,
                     RISK_LEVEL level);

  void ShowSalaryInfo(void) const;

  int GetRiskPay(void) const;

private: // NOLINT
  RISK_LEVEL level;
};

class EmployeeHandler {
public: // NOLINT
  EmployeeHandler();
  ~EmployeeHandler();

  void AddEmployee(Employee *emp);
  void ShowAllSalaryInfo(void) const;
  void ShowTotalSalary(void) const;

private: // NOLINT
  Employee *empList[50];
  int empNum;
};

///////////////////////////////////////////////////////////////////////////////

Employee::Employee(const char *name) {
  snprintf(this->name, strlen(name) + 1, "%s", name);
}

Employee::~Employee() {}

void Employee::ShowYourName(void) const {
  std::cout << "name: " << name << std::endl;
}

int Employee::GetPay(void) const { return 0; }

void Employee::ShowSalaryInfo(void) const {}

///////////////////////////////////////////////////////////////////////////////

PermanentWorker::PermanentWorker(std::string name, int money)
    : Employee(name.c_str()), salary(money) {}

int PermanentWorker::GetPay(void) const { return salary; }

void PermanentWorker::ShowSalaryInfo(void) const {
  ShowYourName();
  std::cout << "Salary: " << GetPay() << std::endl;
}

///////////////////////////////////////////////////////////////////////////////

TemporaryWorker::TemporaryWorker(std::string name, int pay)
    : Employee(name.c_str()), workTime(0), payPerHour(pay) {}

void TemporaryWorker::AddWorkTime(int time) { workTime += time; }

int TemporaryWorker::GetPay(void) const { return workTime * payPerHour; }

void TemporaryWorker::ShowSalaryInfo(void) const {
  ShowYourName();
  std::cout << "salary: " << GetPay() << std::endl;
}

///////////////////////////////////////////////////////////////////////////////

SalesWorker::SalesWorker(std::string name, int money, double ratio)
    : PermanentWorker(name, money), salesResult(0), bonusRatio(ratio) {}

void SalesWorker::AddSalesResult(int value) { salesResult += value; }

int SalesWorker::GetPay(void) const {
  return PermanentWorker::GetPay() + static_cast<int>(salesResult * bonusRatio);
}

void SalesWorker::ShowSalaryInfo(void) const {
  ShowYourName();
  std::cout << "Salary: " << GetPay() << std::endl;
}

///////////////////////////////////////////////////////////////////////////////

ForeignSalesWorker::ForeignSalesWorker(std::string name, int money,
                                       double ratio, RISK_LEVEL level)
    : SalesWorker(name, money, ratio), level(level) {}

void ForeignSalesWorker::ShowSalaryInfo(void) const {
  SalesWorker::ShowSalaryInfo();
  std::cout << "risk pay: " << GetRiskPay() << std::endl;
  std::cout << "sum: " << SalesWorker::GetPay() + GetRiskPay() << std::endl
            << std::endl;
}

int ForeignSalesWorker::GetRiskPay(void) const {
  return SalesWorker::GetPay() * static_cast<int>(this->level) / 10;
}

///////////////////////////////////////////////////////////////////////////////

EmployeeHandler::EmployeeHandler() : empNum(0) {}

EmployeeHandler::~EmployeeHandler() {
  for (int i = 0; i < empNum; i++)
    delete empList[i];
}

void EmployeeHandler::AddEmployee(Employee *emp) { empList[empNum++] = emp; }

void EmployeeHandler::ShowAllSalaryInfo(void) const {
  for (int i = 0; i < empNum; i++)
    empList[i]->ShowSalaryInfo();
}

void EmployeeHandler::ShowTotalSalary(void) const {
  int sum = 0;
  for (int i = 0; i < empNum; i++)
    sum += empList[i]->GetPay();

  std::cout << "Salary sum: " << sum << std::endl;
}

///////////////////////////////////////////////////////////////////////////////

#if 0
int main(void) {
  EmployeeHandler handler;

  handler.AddEmployee(new PermanentWorker("Kim", 1000));
  handler.AddEmployee(new PermanentWorker("Lee", 1500));
  handler.AddEmployee(new PermanentWorker("Jun", 2000));

  TemporaryWorker *alba = new TemporaryWorker("Jung", 700);
  alba->AddWorkTime(5);
  handler.AddEmployee(alba);

  SalesWorker *seller = new SalesWorker("Hong", 1000, 0.1);
  seller->AddSalesResult(7000);
  handler.AddEmployee(seller);

  handler.ShowAllSalaryInfo();

  handler.ShowTotalSalary();
  return 0;
}
#else
int main(void) {
  EmployeeHandler handler;

  ForeignSalesWorker *fseller1 =
      new ForeignSalesWorker("Hong", 1000, 0.1, RISK_LEVEL::RISK_A);
  fseller1->AddSalesResult(7000);
  handler.AddEmployee(fseller1);

  ForeignSalesWorker *fseller2 =
      new ForeignSalesWorker("Yoon", 1000, 0.1, RISK_LEVEL::RISK_B);
  fseller2->AddSalesResult(7000);
  handler.AddEmployee(fseller2);

  ForeignSalesWorker *fseller3 =
      new ForeignSalesWorker("Lee", 1000, 0.1, RISK_LEVEL::RISK_C);
  fseller3->AddSalesResult(7000);
  handler.AddEmployee(fseller3);

  handler.ShowAllSalaryInfo();
  return 0;
}
#endif
