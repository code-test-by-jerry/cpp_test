/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <iostream>

static inline int BoxVolume(int length, int width, int height);
static inline int BoxVolume(int length, int width);
static inline int BoxVolume(int length);

static inline int BoxVolume(int length, int width, int height) {
  return length * width * height;
}
static inline int BoxVolume(int length, int width) { return length * width; }
static inline int BoxVolume(int length) { return length; }

int main(void) {
  std::cout << "[3, 3, 3] : " << BoxVolume(3, 3, 3) << std::endl;
  std::cout << "[5, 5, D] : " << BoxVolume(5, 5) << std::endl;
  std::cout << "[7, D, D] : " << BoxVolume(7) << std::endl;

  return 0;
}
