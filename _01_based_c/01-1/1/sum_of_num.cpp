/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <iostream>

int main(void) {
  int num;
  int sum = 0;
  for (int i = 0; i < 5; i++) {
    std::cout << "Input the number: ";
    std::cin >> num;
    sum += num;
  }

  std::cout << "Sum: " << sum << std::endl;
  return 0;
}
