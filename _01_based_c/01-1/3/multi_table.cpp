/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <iostream>

int main(void) {
  int number;
  std::cout << "Input Number: ";
  std::cin >> number;

  for (int i = 1; i < 10; i++) {
    std::cout << number << " * " << i << " = " << number * i << std::endl;
  }

  return 0;
}
