/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <iostream>

static inline int InputNumber() {
  int number;
  std::cout << "판매 금액을 만원단위로 입력(-1 to end): ";
  std::cin >> number;
  return number;
}

static inline double CalculateSalary(const int sell) {
  return (sell * 0.12) + 50;
}

int main(void) {
  int number;
  int salary;

  while (true) {
    number = InputNumber();
    if (number == -1)
      break;

    salary = CalculateSalary(number);
    std::cout << "이번달 급여: " << salary << "만원" << std::endl;
  }

  std::cout << "프로그램을 종료합니다." << std::endl;
  return 0;
}
