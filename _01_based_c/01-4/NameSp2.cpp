/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <iostream>

#include "NameSp2Func.h"

int main(void) {
  BestComImpl::SimpleFunc();
  ProgComImpl::SimpleFunc();

  return 0;
}
