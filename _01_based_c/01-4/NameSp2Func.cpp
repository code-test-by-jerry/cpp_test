/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <iostream>

#include "NameSp2Func.h"

void BestComImpl::SimpleFunc(void) {
  std::cout << "BestCom이 정의한 함수" << std::endl;
}

void ProgComImpl::SimpleFunc(void) {
  std::cout << "ProgCom이 정의한 함수" << std::endl;
}
