/* Copyright 2021. Lee, Jerry J all rights reserved */

#ifndef _01_BASED_C_01_4_NAMESP2FUNC_H_
#define _01_BASED_C_01_4_NAMESP2FUNC_H_

namespace BestComImpl {
void SimpleFunc(void);
};

namespace ProgComImpl {
void SimpleFunc(void);
};

#endif /* _01_BASED_C_01_4_NAMESP2FUNC_H_ */
