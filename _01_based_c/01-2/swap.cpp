/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <iostream>

#define PTR_GUARD(ptr_1, ptr_2)                                                \
  if (ptr_1 == NULL || ptr_2 == NULL) {                                        \
    std::cout << "Invalid address! " << __func__ << std::endl;                 \
    return;                                                                    \
  }

static inline void _swap(int *num1, int *num2);
static inline void _swap(char *ch1, char *ch2);
static inline void _swap(double *dbl1, double *dbl2);

static inline void _swap(int *num1, int *num2) {
  PTR_GUARD(num1, num2);

  int temp = *num1;
  *num1 = *num2;
  *num2 = temp;
}

static inline void _swap(char *ch1, char *ch2) {
  PTR_GUARD(ch1, ch2);

  char temp = *ch1;
  *ch1 = *ch2;
  *ch2 = temp;
}

static inline void _swap(double *dbl1, double *dbl2) {
  PTR_GUARD(dbl1, dbl2);

  double temp = *dbl1;
  *dbl1 = *dbl2;
  *dbl2 = temp;
}

int main(void) {
  int num1 = 20, num2 = 30;
  _swap(&num1, &num2);
  std::cout << num1 << " " << num2 << std::endl;

  char ch1 = 'A', ch2 = 'B';
  _swap(&ch1, &ch2);
  std::cout << ch1 << " " << ch2 << std::endl;

  double dbl1 = 1.111, dbl2 = 5.555;
  _swap(&dbl1, &dbl2);
  std::cout << dbl1 << " " << dbl2 << std::endl;

  return 0;
}
