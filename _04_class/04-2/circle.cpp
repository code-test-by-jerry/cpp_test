/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <iostream>

class Point {
public: // NOLINT
  Point();
  Point(int x, int y);
  void ShowPointInfo(void) const;

private: // NOLINT
  int xpos, ypos;
};

Point::Point() {}
Point::Point(int x, int y) : xpos(x), ypos(y) {}
void Point::ShowPointInfo(void) const {
  std::cout << "[" << xpos << ", " << ypos << "]" << std::endl;
}

///////////////////////////////////////////////////////////////////////////////

class Circle {
public: // NOLINT
  Circle();
  Circle(const Point &point, int radius);
  void ShowCircleInfo(void) const;

private: // NOLINT
  Point point;
  int radius;
};

Circle::Circle() {}
Circle::Circle(const Point &point, int radius) : point(point), radius(radius) {}

void Circle::ShowCircleInfo(void) const {
  std::cout << "radius: " << radius << std::endl;
  point.ShowPointInfo();
}

///////////////////////////////////////////////////////////////////////////////

class Ring {
public: // NOLINT
  Ring(int inX, int inY, int inRadius, int outX, int outY, int outRadius);
  void ShowRingInfo(void) const;

private: // NOLINT
  Circle inCir;
  Circle outCir;
};

Ring::Ring(int inX, int inY, int inRadius, int outX, int outY, int outRadius) {
  Point inPoint(inX, inY);
  Circle inCircle(inPoint, inRadius);

  Point outPoint(outX, outY);
  Circle outCircle(outPoint, outRadius);

  this->inCir = inCircle;
  this->outCir = outCircle;
}

void Ring::ShowRingInfo(void) const {
  std::cout << "Innner Circle Info" << std::endl;
  inCir.ShowCircleInfo();

  std::cout << "Outter Circle Info" << std::endl;
  outCir.ShowCircleInfo();
}

///////////////////////////////////////////////////////////////////////////////

int main(void) {
  Ring ring(1, 1, 4, 2, 2, 9);
  ring.ShowRingInfo();

  return 0;
}
