/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <iostream>

enum class COMP_POS { CLERK, SENIOR, ASSIST, MANAGER };

class NameCard {
public: // NOLINT
  NameCard();
  NameCard(std::string name, std::string com, std::string phone, COMP_POS pos);

  void ShowNameCardInfo(void) const;

private: // NOLINT
  std::string name;
  std::string company;
  std::string phoneNum;
  COMP_POS position;
};

NameCard::NameCard() {}
NameCard::NameCard(std::string name, std::string com, std::string phone,
                   COMP_POS pos)
    : name(name), company(com), phoneNum(phone), position(pos) {}

void NameCard::ShowNameCardInfo(void) const {
  const char *posTable[] = {"사원", "주임", "대리", "과장"};

  std::cout << "이름: " << name << std::endl;
  std::cout << "회사: " << company << std::endl;
  std::cout << "전화번호: " << phoneNum << std::endl;
  std::cout << "직급: " << posTable[static_cast<int>(position)] << std::endl;

  std::cout << std::endl;
}

int main(void) {
  NameCard manClerk("Lee", "ABCEng", "010-1111-2222", COMP_POS::CLERK);
  NameCard manSenior("Hong", "OrangEng", "010-3333-4444", COMP_POS::SENIOR);
  NameCard manAssist("Kim", "SoGoodComp", "010-5555-6666", COMP_POS::ASSIST);

  manClerk.ShowNameCardInfo();
  manSenior.ShowNameCardInfo();
  manAssist.ShowNameCardInfo();

  return 0;
}
