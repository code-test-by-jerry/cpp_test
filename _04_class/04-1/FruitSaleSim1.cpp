/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <iostream>

class FruitSeller {
public: // NOLINT
  FruitSeller(int price, int num, int money);

  int SaleApples(int money);
  void ShowSalesResult(void) const;
  int getApplePrice(void) const;

private: // NOLINT
  const int APPLE_PRICE;

  int numOfApples;
  int myMoney;
};

class FruitBuyer {
public: // NOLINT
  explicit FruitBuyer(int money);

  void BuyApples(FruitSeller *seller, int money);
  void ShowBuyResult(void) const;

private: // NOLINT
  int numOfApples;
  int myMoney;
};

FruitSeller::FruitSeller(int price, int num, int money)
    : APPLE_PRICE(price), numOfApples(num), myMoney(money) {}

int FruitSeller::SaleApples(int money) {
  int num = money / APPLE_PRICE;
  numOfApples -= num;
  myMoney += money;
  return num;
}

void FruitSeller::ShowSalesResult(void) const {
  std::cout << "남은 사과: " << numOfApples << std::endl;
  std::cout << "판매 수익: " << myMoney << std::endl;
}

int FruitSeller::getApplePrice(void) const { return APPLE_PRICE; }

FruitBuyer::FruitBuyer(int money) : numOfApples(0), myMoney(money) {}

void FruitBuyer::BuyApples(FruitSeller *seller, int money) {
  if (money < seller->getApplePrice()) {
    std::cout << "Failed to buy the apple!" << std::endl;
    return;
  }

  numOfApples += seller->SaleApples(money);
  myMoney -= money;
}

void FruitBuyer::ShowBuyResult(void) const {
  std::cout << "현재 잔액: " << myMoney << std::endl;
  std::cout << "사과 개수: " << numOfApples << std::endl;
}

int main(void) {
  FruitSeller seller(1000, 20, 0);
  FruitBuyer buyer(5000);

  buyer.BuyApples(&seller, 2000);
  buyer.BuyApples(&seller, 500);

  std::cout << "과일 판매자의 현황" << std::endl;
  seller.ShowSalesResult();

  std::cout << "과일 구매자의 현황" << std::endl;
  buyer.ShowBuyResult();

  return 0;
}
