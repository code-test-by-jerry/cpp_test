/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <iostream>

typedef struct __Point {
  int xpos;
  int ypos;
} Point;

Point &PntAdder(const Point &p1, const Point &p2) {
  Point *ret = new Point;
  ret->xpos = p1.xpos + p2.xpos;
  ret->ypos = p1.ypos + p2.ypos;

  return *ret;
}

int main(void) {
  Point *p1 = new Point;
  Point *p2 = new Point;
  if (p1 == NULL || p2 == NULL) {
    std::cout << "Failed to allocate memory!" << std::endl;
    return -1;
  }

  p1->xpos = 1;
  p1->ypos = 2;

  p2->xpos = 3;
  p2->ypos = 4;

  Point &ret = PntAdder(*p1, *p2);
  std::cout << ret.xpos << " " << ret.ypos << std::endl;

  delete p2;
  delete p1;
  delete &ret;

  return 0;
}
