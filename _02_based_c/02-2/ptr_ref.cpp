/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <iostream>

int main(void) {
  const int num = 12;

  const int *ptr = &num;
  const int &ref = num;

  std::cout << "num: " << num << std::endl;
  std::cout << "ptr: " << *ptr << std::endl;
  std::cout << "ref: " << ref << std::endl;

  return 0;
}
