/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <iostream>

static inline void IncreaseNum(int *num) { (*num)++; }
static inline void ChangeSign(int *num) { *num = ~*num + 1; }

// static inline void IncreaseNum(int &num) { num++; }
// static inline void ChangeSign(int &num) { num = ~num + 1; }

int main(void) {
  int num = 4;
  std::cout << "Pre: " << num << std::endl;
  IncreaseNum(&num);
  std::cout << "After: " << num << std::endl;

  ChangeSign(&num);
  std::cout << "Sign: " << num << std::endl;

  ChangeSign(&num);
  std::cout << "Sign: " << num << std::endl;

  return 0;
}
