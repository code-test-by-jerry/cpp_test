/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <iostream>

#define PTR_GUARD(ptr1, ptr2)                                                  \
  if (ptr1 == NULL || ptr2 == NULL || *ptr1 == NULL || *ptr2 == NULL) {        \
    std::cout << "Invalild Pointer!" << std::endl;                             \
    return;                                                                    \
  }

static inline void SwapPtr(int **ptr1, int **ptr2) {
  PTR_GUARD(ptr1, ptr2);

  int *temp_ptr = *ptr1;
  *ptr1 = *ptr2;
  *ptr2 = temp_ptr;
}

int main(void) {
  int num1 = 5;
  int num2 = 10;

  int *ptr1 = &num1;
  int *ptr2 = &num2;

  std::cout << *ptr1 << " " << *ptr2 << std::endl;
  SwapPtr(&ptr1, &ptr2);
  std::cout << *ptr1 << " " << *ptr2 << std::endl;

  return 0;
}
