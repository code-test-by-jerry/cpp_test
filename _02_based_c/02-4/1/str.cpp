/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <cstring>
#include <iostream>

int main(void) {
  std::string str1 = "Lee, Jerry J";
  std::string str2 = ", jslee24x@gmail.com";

  std::cout << "len: " << strlen(str1.c_str()) << std::endl;

  char strSum[1024] = {
      0,
  };

  snprintf(strSum, strlen(str1.c_str()) + 1, "%s", str1.c_str());
  snprintf(&strSum[strlen(strSum)], strlen(str2.c_str()) + 1, "%s",
           str2.c_str());

  //  strcat(strSum, str1.c_str());
  //  strcat(&strSum[strlen(strSum)], str2.c_str());
  std::cout << "append: " << strSum << std::endl;

  return 0;
}
