/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <cstdlib>
#include <ctime>
#include <iostream>

int main(void) {
  srand(time(NULL));

  int i;
  for (i = 0; i < 5; i++) {
    std::cout << "Random Value: " << rand() % 100 << std::endl; // NOLINT
  }

  return 0;
}
