/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <iostream>

class Point {
public: // NOLINT
  Point(int x, int y);
  void ShowPosition(void) const;

  Point &operator++(void);
  Point &operator--(void);

  Point &operator-(void);
  Point &operator~(void);

private: // NOLINT
  int xpos;
  int ypos;
};

Point::Point(int x = 0, int y = 0) : xpos(x), ypos(y) {}

void Point::ShowPosition(void) const {
  std::cout << "[" << xpos << "," << ypos << "]" << std::endl;
}

Point &Point::operator++(void) {
  this->xpos += 1;
  this->ypos += 1;
  return *this;
}

Point &Point::operator--(void) {
  this->xpos -= 1;
  this->ypos -= 1;
  return *this;
}

Point &Point::operator-(void) {
  this->xpos = -this->xpos;
  this->ypos = -this->ypos;
  return *this;
}

Point &Point::operator~(void) {
  this->xpos = ~this->xpos;
  this->ypos = ~this->ypos;
  return *this;
}

int main(void) {
  Point pos(0, 200);
  ++pos;
  pos.ShowPosition();
  --pos;
  pos.ShowPosition();

  ++(++pos);
  pos.ShowPosition();
  --(--pos);
  pos.ShowPosition();

  Point pos2 = -pos;
  pos2.ShowPosition();

  Point pos3 = ~pos;
  pos3.ShowPosition();

  return 0;
}
