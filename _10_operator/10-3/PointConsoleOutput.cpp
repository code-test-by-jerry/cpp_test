/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <iostream>

class Point {
public: // NOLINT
  Point(int x, int y);

  void ShowPosition(void) const;

  friend std::ostream &operator<<(std::ostream &os, const Point &pos);
  friend std::istream &operator>>(std::istream &is, Point &pos);

private: // NOLINT
  int xpos;
  int ypos;
};

Point::Point(int x = 0, int y = 0) : xpos(x), ypos(y) {}

void Point::ShowPosition(void) const {
  std::cout << "[" << xpos << "," << ypos << "]" << std::endl;
}

std::ostream &operator<<(std::ostream &os, const Point &pos) {
  os << "[" << pos.xpos << ", " << pos.ypos << "]" << std::endl;
  return os;
}

std::istream &operator>>(std::istream &is, Point &pos) {
  is >> pos.xpos >> pos.ypos;
  return is;
}

int main(void) {
  Point pos1;
  std::cout << "x, y 좌표 순으로 입력: ";
  std::cin >> pos1;
  std::cout << pos1;

  Point pos2;
  std::cout << "x, y 좌표 순으로 입력: ";
  std::cin >> pos2;
  std::cout << pos2;

  return 0;
}
