/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <iostream>

class Point {
public: // NOLINT
  Point(int x, int y);
  void ShowPosition(void) const;

  friend Point operator+(const Point &pos1, const Point &pos2);
  friend Point operator-(const Point &pos1, const Point &pos2);

  Point &operator+=(const Point &pos);
  Point &operator-=(const Point &pos);

private: // NOLINT
  int xpos;
  int ypos;
};

Point::Point(int x = 0, int y = 0) : xpos(x), ypos(y) {}

void Point::ShowPosition(void) const {
  std::cout << "[" << xpos << "," << ypos << "]" << std::endl;
}

Point operator+(const Point &pos1, const Point &pos2) {
  Point pos(pos1.xpos + pos2.xpos, pos1.ypos + pos2.ypos);
  return pos;
}

Point operator-(const Point &pos1, const Point &pos2) {
  Point pos(pos1.xpos - pos2.xpos, pos1.ypos - pos2.ypos);
  return pos;
}

Point &Point::operator+=(const Point &pos) {
  this->xpos += pos.xpos;
  this->ypos += pos.ypos;
  return *this;
}

Point &Point::operator-=(const Point &pos) {
  this->xpos -= pos.xpos;
  this->ypos -= pos.ypos;
  return *this;
}

int main(void) {
  Point pos1(3, 4);
  Point pos2(10, 20);
  Point pos3 = pos1 + pos2;
  Point pos4 = pos1 - pos2;

  Point pos5(20, 30);
  pos5 += pos1;

  Point pos6(100, 200);
  pos6 -= pos2;

  pos1.ShowPosition();
  pos2.ShowPosition();
  pos3.ShowPosition();
  pos4.ShowPosition();

  pos5.ShowPosition();
  pos6.ShowPosition();

  return 0;
}
