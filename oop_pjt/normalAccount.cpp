/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <normalAccount.h>

NormalAccount::NormalAccount(int32_t accID, int32_t balance,
                             const char *cusName, int32_t rate)
    : Account(accID, balance, cusName), interestRate(rate) {}

void NormalAccount::DepositMoney(int32_t money) {
  Account::DepositMoney(money + (money * (interestRate / 100.0)));
}
