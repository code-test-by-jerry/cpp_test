/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <climits>

#include <accountArray.h>
#include <highCreditAccount.h>
#include <normalAccount.h>

class AccountHandler {
public: // NOLINT
  explicit AccountHandler(int32_t accNum);
  ~AccountHandler();

  void ShowMenu(void) const;
  void MakeAccount(void);

  void DepositMoney(void);
  void WithdrawMoney(void);
  void ShowAllAccInfo(void) const;

private: // NOLINT
  BoundCheckAccountPtrArray mAccount;
  int32_t mAccNum;
};

AccountHandler::AccountHandler(int32_t accNum) : mAccNum(accNum) {}

AccountHandler::~AccountHandler() {
  for (int32_t i = 0; i < mAccNum; i++) {
    if (mAccount[i]) {
      delete mAccount[i];
      mAccount[i] = nullptr;
    }
  }
}

void AccountHandler::ShowMenu(void) const {
  std::cout << std::endl;
  std::cout << "--------- Menu ---------" << std::endl;
  std::cout << "1. 계좌개설" << std::endl;
  std::cout << "2. 입    금" << std::endl;
  std::cout << "3. 출    금" << std::endl;
  std::cout << "4. 계좌정보" << std::endl;
  std::cout << "5. 종    료" << std::endl << std::endl;
}

void AccountHandler::MakeAccount(void) {
  if (this->mAccNum >= 100) {
    std::cout << "계좌 개설이 불가능 합니다." << std::endl;
    return;
  }

  std::string name;
  int32_t kind, id, balance, rate;

  std::cout << "[계좌종류선택]" << std::endl;
  std::cout << "1. 보통예금계좌 2. 신용신뢰계좌" << std::endl;
  std::cout << "선택: ";
  std::cin >> kind;
  if (!(kind == 1 || kind == 2)) {
    std::cout << "계좌 종류 선택 오류!" << std::endl;
    return;
  }

  std::cout << "[계좌개설]" << std::endl;
  std::cout << "계좌ID: ";
  std::cin >> id;

  std::cout << "이름: ";
  std::cin.ignore(100, '\n');
  std::getline(std::cin, name, '\n');

  std::cout << "입금액: ";
  std::cin >> balance;

  std::cout << "이자율: ";
  std::cin >> rate;

  if (kind == 1) {
    this->mAccount[this->mAccNum] =
        new NormalAccount(id, balance, name.c_str(), rate);
  } else {
    int32_t level;
    std::cout << "신용등급(1toA, 2toB, 3toC): ";
    std::cin >> level;
    this->mAccount[this->mAccNum] =
        new HighCreditAccount(id, balance, name.c_str(), rate, level);
  }

  this->mAccNum++;
}

void AccountHandler::DepositMoney(void) {
  int32_t money;
  int32_t id;

  std::cout << "[입    금]" << std::endl;

  std::cout << "계좌ID: ";
  std::cin >> id;

  std::cout << "입금액: ";
  std::cin >> money;

  Account *curAcc = nullptr;
  for (int32_t i = 0; i < this->mAccNum; i++) {
    curAcc = this->mAccount[i];
    if (curAcc->GetAccID() == id) {
      curAcc->DepositMoney(money);
      std::cout << "입금완료" << std::endl;
      return;
    }
  }

  std::cout << "유효하지 않은 ID 입니다." << std::endl << std::endl;
}

void AccountHandler::WithdrawMoney(void) {
  int32_t money;
  int32_t id;

  std::cout << "[출    금]" << std::endl;

  std::cout << "계좌ID: ";
  std::cin >> id;

  std::cout << "출금액: ";
  std::cin >> money;

  int32_t ret = -EPERM;
  Account *curAcc = nullptr;
  for (int32_t i = 0; i < this->mAccNum; i++) {
    curAcc = this->mAccount[i];
    if (curAcc->GetAccID() == id) {
      ret = curAcc->WithdrawMoney(money);
      if (!ret)
        std::cout << "출금완료" << std::endl << std::endl;

      return;
    }
  }

  std::cout << "유효하지 않은 ID 입니다." << std::endl << std::endl;
}

void AccountHandler::ShowAllAccInfo(void) const {
  for (int32_t i = 0; i < this->mAccNum; i++) {
    this->mAccount[i]->ShowAllAccInfo();
  }
}

int32_t main(void) {
  int32_t choice;
  AccountHandler accountHandler(0);

  while (1) {
    accountHandler.ShowMenu();
    std::cout << "선택: ";
    std::cin >> choice;
    std::cout << std::endl;

    switch (static_cast<eMENU>(choice)) {
    case eMENU::MAKE:
      accountHandler.MakeAccount();
      break;

    case eMENU::DEPOSIT:
      accountHandler.DepositMoney();
      break;

    case eMENU::WITHDRAW:
      accountHandler.WithdrawMoney();
      break;

    case eMENU::INQUIRE:
      accountHandler.ShowAllAccInfo();
      break;

    case eMENU::EXIT:
      return 0;

    default:
      std::cout << "Illegal selection." << std::endl;
      break;
    }
  }

  return 0;
}
