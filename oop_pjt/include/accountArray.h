/* Copyright 2021. Lee, Jerry J all rights reserved */

#ifndef OOP_PJT_INCLUDE_ACCOUNTARRAY_H_
#define OOP_PJT_INCLUDE_ACCOUNTARRAY_H_

#include <account.h>

#include <cstdlib>

typedef Account *ACCOUNT_PTR;

class BoundCheckAccountPtrArray {
public: // NOLINT
  BoundCheckAccountPtrArray();
  explicit BoundCheckAccountPtrArray(int32_t len);
  ~BoundCheckAccountPtrArray();

  ACCOUNT_PTR &operator[](int32_t idx);
  ACCOUNT_PTR operator[](int32_t idx) const;

  int32_t GetArrLen(void) const;

private: // NOLINT
  ACCOUNT_PTR *arr;
  int32_t arrLen;
};

#endif /* OOP_PJT_INCLUDE_ACCOUNTARRAY_H_ */ // NOLINT
