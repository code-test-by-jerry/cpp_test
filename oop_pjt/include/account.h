/* Copyright 2021. Lee, Jerry J all rights reserved */

#ifndef OOP_PJT_INCLUDE_ACCOUNT_H_
#define OOP_PJT_INCLUDE_ACCOUNT_H_

#include <cerrno>
#include <cstring>
#include <iostream>

enum class eMENU { MAKE = 1, DEPOSIT, WITHDRAW, INQUIRE, EXIT };

class Account {
public: // NOLINT
  Account() = delete;
  Account(int32_t accID, int32_t balance, const char *cusName);
  virtual ~Account(void);

  Account &operator=(const Account &acc);

  virtual void DepositMoney(int32_t money);

  int32_t WithdrawMoney(int32_t money);
  void ShowAllAccInfo(void) const;
  int32_t GetAccID(void);

private: // NOLINT
  int32_t mAccID = 0;
  int32_t mBalance = 0;
  char *mCusName = nullptr;
};

#endif /* OOP_PJT_INCLUDE_ACCOUNT_H_ */ // NOLINT
