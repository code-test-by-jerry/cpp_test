/* Copyright 2021. Lee, Jerry J all rights reserved */

#ifndef OOP_PJT_INCLUDE_NORMALACCOUNT_H_
#define OOP_PJT_INCLUDE_NORMALACCOUNT_H_

#include <account.h>

class NormalAccount : public Account {
public: // NOLINT
  NormalAccount() = delete;
  NormalAccount(int32_t accID, int32_t balance, const char *cusName,
                int32_t rate);

  void DepositMoney(int32_t momey) override;

private: // NOLINT
  int32_t interestRate;
};

#endif /* OOP_PJT_INCLUDE_NORMALACCOUNT_H_ */ // NOLINT
