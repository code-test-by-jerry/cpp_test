/* Copyright 2021. Lee, Jerry J all rights reserved */

#ifndef OOP_PJT_INCLUDE_HIGHCREDITACCOUNT_H_
#define OOP_PJT_INCLUDE_HIGHCREDITACCOUNT_H_

#include <normalAccount.h>

class HighCreditAccount : public NormalAccount {
public: // NOLINT
  HighCreditAccount() = delete;
  HighCreditAccount(int32_t accID, int32_t balance, const char *cusName,
                    int32_t rate, int32_t level);

  void DepositMoney(int32_t money) override;

private: // NOLINT
  int32_t creditLevel;
};

#endif /* OOP_PJT_INCLUDE_HIGHCREDITACCOUNT_H_ */ // NOLINT
