/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <highCreditAccount.h>

HighCreditAccount::HighCreditAccount(int32_t accID, int32_t balance,
                                     const char *cusName, int32_t rate,
                                     int32_t level)
    : NormalAccount(accID, balance, cusName, rate), creditLevel(level) {}

void HighCreditAccount::DepositMoney(int32_t money) {
  const int32_t ratio[] = {7, 4, 2};

  NormalAccount::DepositMoney(money);
  Account::DepositMoney(money * (ratio[this->creditLevel] / 100.0));
}
