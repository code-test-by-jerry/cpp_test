/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <account.h>

Account::Account(int32_t accID, int32_t balance, const char *cusName)
    : mAccID(accID), mBalance(balance) {
  size_t cusNameLen = strlen(cusName) + 1;
  this->mCusName = new char[cusNameLen];
  snprintf(this->mCusName, cusNameLen, "%s", cusName);
}

Account::~Account(void) {
  if (this->mCusName) {
    delete[] this->mCusName;
    this->mCusName = nullptr;
  }
}

Account &Account::operator=(const Account &acc) {
  this->mAccID = acc.mAccID;
  this->mBalance = acc.mBalance;

  if (this->mCusName)
    delete[] this->mCusName;

  size_t cusNameLen = strlen(acc.mCusName) + 1;
  this->mCusName = new char[cusNameLen];
  snprintf(this->mCusName, cusNameLen, "%s", acc.mCusName);

  return *this;
}

void Account::DepositMoney(int32_t money) { mBalance += money; }

int32_t Account::WithdrawMoney(int32_t money) {
  if (mBalance < money) {
    std::cout << "잔액부족" << std::endl << std::endl;
    return -EPERM;
  }

  mBalance -= money;
  return 0;
}

void Account::ShowAllAccInfo(void) const {
  std::cout << "계좌ID: " << mAccID << std::endl;
  std::cout << "이름: " << mCusName << std::endl;
  std::cout << "잔액: " << mBalance << std::endl << std::endl;
}

int32_t Account::GetAccID(void) { return (const int32_t)mAccID; }
