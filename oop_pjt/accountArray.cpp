/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <accountArray.h>

BoundCheckAccountPtrArray::BoundCheckAccountPtrArray() : arrLen(100) {
  arr = new ACCOUNT_PTR[this->arrLen];
}

BoundCheckAccountPtrArray::BoundCheckAccountPtrArray(int32_t len)
    : arrLen(len) {
  arr = new ACCOUNT_PTR[len];
}

BoundCheckAccountPtrArray::~BoundCheckAccountPtrArray() { delete[] arr; }

ACCOUNT_PTR &BoundCheckAccountPtrArray::operator[](int32_t idx) {
  if (idx < 0 || idx >= arrLen) {
    std::cout << "Array index out of bound exception" << std::endl;
    exit(1);
  }

  return arr[idx];
}

ACCOUNT_PTR BoundCheckAccountPtrArray::operator[](int32_t idx) const {
  if (idx < 0 || idx >= arrLen) {
    std::cout << "Array index out of bound exception" << std::endl;
    exit(1);
  }

  return arr[idx];
}

int32_t BoundCheckAccountPtrArray::GetArrLen(void) const { return arrLen; }
