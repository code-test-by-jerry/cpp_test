/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <iostream>

class Point {
public: // NOLINT
  Point(int x, int y);
  void MovePos(int x, int y);
  void AddPoint(const Point &pos);
  void ShowPosition() const;

private: // NOLINT
  int xpos;
  int ypos;
};

Point::Point(int x, int y) : xpos(x), ypos(y) {}

void Point::MovePos(int x, int y) {
  xpos += x;
  ypos += y;
}

void Point::AddPoint(const Point &pos) {
  xpos += pos.xpos;
  ypos += pos.ypos;
}

void Point::ShowPosition() const {
  std::cout << xpos << ", " << ypos << std::endl;
}

int main(void) {
  Point pos1(12, 4);
  Point pos2(20, 30);

  pos1.MovePos(-7, 10);
  pos1.ShowPosition();

  pos1.AddPoint(pos2);
  pos1.ShowPosition();

  return 0;
}
