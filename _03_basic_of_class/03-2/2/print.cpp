/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <iostream>

class Printer {
public: // NOLINT
  void SetString(const std::string str);
  void ShowString(void) const;

private: // NOLINT
  std::string mStr;
};

void Printer::SetString(const std::string str) { mStr = str; }

void Printer::ShowString(void) const { std::cout << mStr << std::endl; }

int main(void) {
  Printer pnt;

  pnt.SetString("Hello World!");
  pnt.ShowString();

  pnt.SetString("I love C++");
  pnt.ShowString();

  return 0;
}
