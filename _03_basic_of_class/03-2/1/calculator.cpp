/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <iostream>

class Calculator {
public: // NOLINT
  Calculator();

  double Add(double num1, double num2);
  double Min(double num1, double num2);
  double Mul(double num1, double num2);
  double Div(double num1, double num2);
  void ShowOpCount() const;

private: // NOLINT
  int AddCnt;
  int MinCnt;
  int MulCnt;
  int DivCnt;
};

Calculator::Calculator() : AddCnt(0), MinCnt(0), MulCnt(0), DivCnt(0) {}

double Calculator::Add(double num1, double num2) {
  AddCnt++;
  return num1 + num2;
}

double Calculator::Min(double num1, double num2) {
  MinCnt++;
  return num1 - num2;
}

double Calculator::Mul(double num1, double num2) {
  MulCnt++;
  return num1 * num2;
}

double Calculator::Div(double num1, double num2) {
  DivCnt++;
  return num1 / num2;
}

void Calculator::ShowOpCount() const {
  std::cout << "Add: " << AddCnt;
  std::cout << ", Min: " << MinCnt;
  std::cout << ", Mul: " << MulCnt;
  std::cout << ", Div: " << DivCnt << std::endl;
}

int main(void) {
  Calculator cal;

  std::cout << "3.2 + 2.4 = " << cal.Add(3.2, 2.4) << std::endl;
  std::cout << "3.5 / 1.7 = " << cal.Div(3.5, 1.7) << std::endl;
  std::cout << "2.2 - 1.5 = " << cal.Min(2.2, 1.5) << std::endl;
  std::cout << "4.9 / 1.2 = " << cal.Div(4.9, 1.2) << std::endl;
  cal.ShowOpCount();

  return 0;
}
