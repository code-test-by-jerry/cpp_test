/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <iostream>

class Gun {
public: // NOLINT
  explicit Gun(int bnum);

  void Shut(void);

private: // NOLINT
  int bullet;
};

class Police {
public: // NOLINT
  Police();
  Police(int bnum, int bcuff);
  Police(const Police &copy);
  ~Police();

  Police &operator=(const Police &police);

  void PutHandcuff(void);
  void Shut(void);

private: // NOLINT
  int handcuffs;
  Gun *pistol;
};

Gun::Gun(int bnum) : bullet(bnum) {}

void Gun::Shut(void) {
  std::cout << "BBANG!" << std::endl;
  bullet--;
}

Police::Police() : handcuffs(0), pistol(NULL) {}

Police::Police(int bnum, int bcuff) : handcuffs(bcuff) {
  if (bnum > 0)
    pistol = new Gun(bnum);
  else
    pistol = NULL;
}

Police::Police(const Police &copy) {
  this->handcuffs = copy.handcuffs;

  if (copy.pistol)
    this->pistol = new Gun(this->handcuffs);
  else
    this->pistol = NULL;
}

Police &Police::operator=(const Police &police) {
  this->handcuffs = police.handcuffs;

  if (police.pistol)
    this->pistol = new Gun(this->handcuffs);
  else
    this->pistol = NULL;

  return *this;
}

Police::~Police() {
  if (pistol != NULL)
    delete pistol;
}

void Police::Shut(void) {
  if (pistol == NULL)
    std::cout << "Hut BBANG!" << std::endl;
  else
    pistol->Shut();
}

void Police::PutHandcuff(void) {
  std::cout << "SNAP!" << std::endl;
  handcuffs--;
}

int main(void) {
  Police pman1(5, 3);
  pman1.Shut();
  pman1.PutHandcuff();

  Police pman2(0, 3);
  pman2.Shut();
  pman2.PutHandcuff();

  Police pman3 = pman2;
  pman3.Shut();
  pman3.PutHandcuff();

  Police pman4;
  pman4 = pman1;
  pman4.Shut();
  pman4.PutHandcuff();

  return 0;
}
