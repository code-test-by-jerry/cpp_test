/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <cstring>
#include <iostream>

class Book {
public: // NOLINT
  Book();
  Book(const char *title, const char *isbn, int price);
  Book(const Book &copy);
  virtual ~Book();

  Book &operator=(const Book &book);

  void ShowBookInfo(void) const;

private: // NOLINT
  char *title;
  char *isbn;
  int price;
};

class EBook : public Book {
public: // NOLINT
  EBook();
  EBook(const char *title, const char *isbn, int price, const char *DRMKey);
  EBook(const EBook &copy);
  virtual ~EBook();

  EBook &operator=(const EBook &ebook);

  void ShowEBookInfo(void) const;

private: // NOLINT
  char *DRMKey;
};

Book::Book() : title(NULL), isbn(NULL), price(0) {}

Book::Book(const char *title, const char *isbn, int price) : price(price) {
  size_t titleLen = strlen(title) + 1;
  size_t isbnLen = strlen(isbn) + 1;

  this->title = new char[titleLen];
  this->isbn = new char[isbnLen];

  snprintf(this->title, titleLen, "%s", title);
  snprintf(this->isbn, isbnLen, "%s", isbn);
}

Book::Book(const Book &copy) {
  if (copy.title) {
    size_t titleLen = strlen(copy.title) + 1;
    this->title = new char[titleLen];
    snprintf(this->title, titleLen, "%s", copy.title);
  }

  if (copy.isbn) {
    size_t isbnLen = strlen(copy.isbn) + 1;
    this->isbn = new char[isbnLen];
    snprintf(this->isbn, isbnLen, "%s", copy.isbn);
  }

  this->price = copy.price;
}

Book::~Book() {
  if (this->title)
    delete[] this->title;

  if (this->isbn)
    delete[] this->isbn;
}

Book &Book::operator=(const Book &book) {
  if (this->title)
    delete[] this->title;

  if (this->isbn)
    delete[] this->isbn;

  size_t titleLen = strlen(book.title) + 1;
  size_t isbnLen = strlen(book.isbn) + 1;

  this->title = new char[titleLen];
  snprintf(this->title, titleLen, "%s", book.title);

  this->isbn = new char[isbnLen];
  snprintf(this->isbn, isbnLen, "%s", book.isbn);

  this->price = book.price;
  return *this;
}

void Book::ShowBookInfo(void) const {
  std::cout << "제목: " << this->title << std::endl;
  std::cout << "ISBN: " << this->isbn << std::endl;
  std::cout << "가격: " << this->price << std::endl;
}

EBook::EBook() : Book(), DRMKey(NULL) {}

EBook::EBook(const char *title, const char *isbn, int price, const char *DRMKey)
    : Book(title, isbn, price) {
  size_t DRMKeyLen = strlen(DRMKey) + 1;
  this->DRMKey = new char[DRMKeyLen];
  snprintf(this->DRMKey, DRMKeyLen, "%s", DRMKey);
}

EBook::EBook(const EBook &copy) : Book(copy) {
  size_t DRMKeyLen = strlen(copy.DRMKey) + 1;
  this->DRMKey = new char[DRMKeyLen];
  snprintf(this->DRMKey, DRMKeyLen, "%s", copy.DRMKey);
}

EBook::~EBook() {
  if (this->DRMKey)
    delete[] this->DRMKey;
}

EBook &EBook::operator=(const EBook &ebook) {
  if (this->DRMKey)
    delete[] this->DRMKey;

  Book::operator=(ebook);
  size_t DRMKeyLen = strlen(ebook.DRMKey) + 1;
  this->DRMKey = new char[DRMKeyLen];
  snprintf(this->DRMKey, DRMKeyLen, "%s", ebook.DRMKey);

  return *this;
}

void EBook::ShowEBookInfo(void) const {
  ShowBookInfo();
  std::cout << "인증키: " << this->DRMKey << std::endl;
}

int main(void) {
  Book book("좋은 C++", "555-12345-890-0", 20000);
  book.ShowBookInfo();

  std::cout << std::endl;

  EBook ebook("좋은 C++", "555-12345-890-1", 10000, "fdx9w0i8kiw");
  ebook.ShowEBookInfo();

  Book book1 = book;
  book1.ShowBookInfo();

  Book book2;
  book2 = book;
  book2.ShowBookInfo();

  EBook eBook1 = ebook;
  eBook1.ShowEBookInfo();

  EBook eBook2;
  eBook2 = ebook;
  eBook2.ShowEBookInfo();

  return 0;
}
