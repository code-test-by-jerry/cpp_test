/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <cstdlib>
#include <iostream>

class Point {
public: // NOLINT
  Point(int x, int y);

  friend std::ostream &operator<<(std::ostream &os, const Point &pos);
  friend std::ostream &operator<<(std::ostream &os, const Point *pos);

private: // NOLINT
  int xpos;
  int ypos;
};

typedef Point *POINT_PTR;

class BoundCheckPointPtrArray {
public: // NOLINT
  explicit BoundCheckPointPtrArray(int len);
  ~BoundCheckPointPtrArray();

  POINT_PTR &operator[](int idx);
  POINT_PTR operator[](int idx) const;

  int GetArrLen(void) const;

private: // NOLINT
  POINT_PTR *arr;
  int arrLen;
};

///////////////////////////////////////////////////////////////////////////////

Point::Point(int x = 0, int y = 0) : xpos(x), ypos(y) {}

std::ostream &operator<<(std::ostream &os, const Point &pos) {
  os << "[" << pos.xpos << ", " << pos.ypos << "]" << std::endl;
  return os;
}

std::ostream &operator<<(std::ostream &os, const Point *pos) {
  os << "[" << pos->xpos << ", " << pos->ypos << "]" << std::endl;
  return os;
}

///////////////////////////////////////////////////////////////////////////////

BoundCheckPointPtrArray::BoundCheckPointPtrArray(int len) : arrLen(len) {
  arr = new POINT_PTR[len];
}

BoundCheckPointPtrArray::~BoundCheckPointPtrArray() { delete[] arr; }

POINT_PTR &BoundCheckPointPtrArray::operator[](int idx) {
  if (idx < 0 || idx >= arrLen) {
    std::cout << "Array index out of bound exception" << std::endl;
    exit(1);
  }

  return arr[idx];
}

POINT_PTR BoundCheckPointPtrArray::operator[](int idx) const {
  if (idx < 0 || idx >= arrLen) {
    std::cout << "Array index out of bound exception" << std::endl;
    exit(1);
  }

  return arr[idx];
}

int BoundCheckPointPtrArray::GetArrLen(void) const { return arrLen; }

///////////////////////////////////////////////////////////////////////////////

int main(void) {
  BoundCheckPointPtrArray arr(3);
  arr[0] = new Point(3, 4);
  arr[1] = new Point(5, 6);
  arr[2] = new Point(7, 8);

  for (int i = 0; i < arr.GetArrLen(); i++)
    std::cout << *(arr[i]);

  std::cout << std::endl;

  for (int i = 0; i < arr.GetArrLen(); i++)
    std::cout << arr[i];

  delete arr[2];
  delete arr[1];
  delete arr[0];

  return 0;
}
