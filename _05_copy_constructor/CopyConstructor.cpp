/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <cstdio>
#include <cstring>
#include <iostream>

enum class COMP_POS { CLERK, SENIOR, ASSIST, MANAGER };

class NameCard {
public: // NOLINT
  NameCard();
  NameCard(const char *name, const char *company, const char *phoneNum,
           COMP_POS pos);
  NameCard(const NameCard &copy);
  virtual ~NameCard();

  void ShowNameCardInfo(void) const;

private: // NOLINT
  char *name;
  char *company;
  char *phoneNum;
  COMP_POS position;
};

NameCard::NameCard() {}
NameCard::NameCard(const char *name, const char *company, const char *phoneNum,
                   COMP_POS pos)
    : position(pos) {
  this->name = new char[strlen(name) + 1];
  snprintf(this->name, strlen(name) + 1, "%s", name);

  this->company = new char[strlen(company) + 1];
  snprintf(this->company, strlen(company) + 1, "%s", company);

  this->phoneNum = new char[strlen(phoneNum) + 1];
  snprintf(this->phoneNum, strlen(phoneNum) + 1, "%s", phoneNum);
}

NameCard::NameCard(const NameCard &copy)
    : phoneNum(copy.phoneNum), position(copy.position) {
  this->name = new char[strlen(copy.name) + 1];
  snprintf(this->name, strlen(copy.name) + 1, "%s", copy.name);

  this->company = new char[strlen(copy.company) + 1];
  snprintf(this->company, strlen(copy.company) + 1, "%s", copy.company);

  this->phoneNum = new char[strlen(copy.phoneNum) + 1];
  snprintf(this->phoneNum, strlen(copy.phoneNum) + 1, "%s", copy.phoneNum);
}

NameCard::~NameCard() {
  if (this->phoneNum)
    delete this->phoneNum;

  if (this->company)
    delete this->company;

  if (this->name)
    delete this->name;
}

void NameCard::ShowNameCardInfo(void) const {
  const char *posTable[] = {"사원", "주임", "대리", "과장"};

  std::cout << "이름: " << name << std::endl;
  std::cout << "회사: " << company << std::endl;
  std::cout << "전화번호: " << phoneNum << std::endl;
  std::cout << "직급: " << posTable[static_cast<int>(position)] << std::endl;

  std::cout << std::endl;
}

int main(void) {
  NameCard manClerk("Lee", "ABCEng", "010-1111-2222", COMP_POS::CLERK);
  manClerk.ShowNameCardInfo();

  NameCard copy1 = manClerk;
  copy1.ShowNameCardInfo();

  NameCard copy2 = copy1;
  copy2.ShowNameCardInfo();

  return 0;
}
