/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <iostream>

class Point {
public: // NOLINT
  Point();
  Point(int x, int y);
  //  Point(const Point &copy);

  void ShowPosition(void) const;

private: // NOLINT
  int xpos;
  int ypos;
};

Point::Point() : xpos(0), ypos(0) {}

Point::Point(int x = 0, int y = 0) : xpos(x), ypos(y) {}

void Point::ShowPosition(void) const {
  std::cout << "[" << xpos << ", " << ypos << "]" << std::endl;
}

template <class Temp> void SwapData(Temp *temp1, Temp *temp2) {
  Temp copy = *temp1;
  *temp1 = *temp2;
  *temp2 = copy;
}

int main(void) {
  Point point1(1, 2);
  Point point2(3, 4);

  point1.ShowPosition();
  point2.ShowPosition();

  SwapData(&point1, &point2);
  std::cout << std::endl;

  point1.ShowPosition();
  point2.ShowPosition();

  return 0;
}
