/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <iostream>

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof(arr[0]))

template <typename T> T SumArray(T arr[], size_t len) {
  T sum = 0;
  for (size_t i = 0; i < len; i++)
    sum += arr[i];

  return sum;
}

int main(void) {
  int intArr[5] = {1, 2, 3, 4, 5};
  double doubleArr[5] = {1.0, 2.0, 3.0, 4.0, 5.0};

  std::cout << SumArray(intArr, ARRAY_SIZE(intArr)) << std::endl;
  std::cout << SumArray(doubleArr, ARRAY_SIZE(doubleArr)) << std::endl;

  return 0;
}
