/* Copyright 2021. Lee, Jerry J all rights reserved */

#include <iostream>

class Point {
public: // NOLINT
  Point();
  Point(int x, int y);

  void SetPos(int x, int y);
  void ShowPosition(void) const;

private: // NOLINT
  int xpos;
  int ypos;
};

template <class T> class SmartPtr {
public: // NOLINT
  SmartPtr() = delete;
  explicit SmartPtr(T *ptr);
  virtual ~SmartPtr();

  T &operator*() const;
  T *operator->() const;

private: // NOLINT
  T *posptr;
};

///////////////////////////////////////////////////////////////////////////////

Point::Point() : xpos(0), ypos(0) {}

Point::Point(int x = 0, int y = 0) : xpos(x), ypos(y) {}

void Point::SetPos(int x, int y) {
  this->xpos = x;
  this->ypos = y;
}

void Point::ShowPosition(void) const {
  std::cout << "[" << xpos << ", " << ypos << "]" << std::endl;
}

///////////////////////////////////////////////////////////////////////////////

template <class T> SmartPtr<T>::SmartPtr(T *ptr) : posptr(ptr) {}

template <class T> SmartPtr<T>::~SmartPtr() { delete posptr; }

template <class T> T &SmartPtr<T>::operator*() const { return *posptr; }

template <class T> T *SmartPtr<T>::operator->() const { return posptr; }

///////////////////////////////////////////////////////////////////////////////

int main(void) {
  SmartPtr<Point> sptr1(new Point(1, 2));
  SmartPtr<Point> sptr2(new Point(3, 4));

  sptr1->ShowPosition();
  sptr2->ShowPosition();

  sptr1->SetPos(10, 20);
  sptr2->SetPos(30, 40);

  sptr1->ShowPosition();
  sptr2->ShowPosition();

  return 0;
}
